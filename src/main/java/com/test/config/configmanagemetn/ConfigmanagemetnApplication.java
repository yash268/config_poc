package com.test.config.configmanagemetn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigmanagemetnApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigmanagemetnApplication.class, args);
	}

}
