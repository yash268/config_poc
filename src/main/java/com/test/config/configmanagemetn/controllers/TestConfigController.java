package com.test.config.configmanagemetn.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config-manager")
@RefreshScope
public class TestConfigController {
	
	@Value("${config1.test}")
	private String config1;
	@Value("${config2.test}")
	private String config2;
	
	
	@GetMapping("/fetchConfig")
	public String fetchConfig() {
		return config1 + " " + config2;
	}

}
